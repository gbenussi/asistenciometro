/**
 * Log.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

function obtenerMinutos(datetime) {
    var moment = require('moment');
    var now = moment(datetime);
    var entrada = moment("09:00:00", 'hh:mm:ss');
    console.log(now.format());
    console.log(entrada.format());

    // var difference_in_minutes = entrada.diff(now, 'minutes')
    var difference_in_minutes = (now.hour() * 60 + now.minute()) - (entrada.hour() * 60 + entrada.minute())
    difference_in_minutes = difference_in_minutes % (24 * 60) // 24*60 = minutos de un dia

    console.log(difference_in_minutes);
    return difference_in_minutes
}

function calcularPuntos(difference_in_minutes, type) {
  if (type == 'ENTRADA') {
    if (difference_in_minutes <= 0) {
      return 20
    }
    if (difference_in_minutes <= 15) {
      return 10
    } else {
      return -10
    }
  }
  return 0
}

module.exports = {

  attributes: {
    worker: {
      model: 'worker'
    },
    type: {
      type: 'string',
      required: true,
      defaultsTo: 'ENTRADA'
    },
    datetime: {
      type: 'datetime',
      defaultsTo: function() {
        return new Date();
      }
    },
  },
  afterCreate: function(newlyInsertedRecord, cb) {
    Worker.findOne({
      id: newlyInsertedRecord.worker
    }).exec(function(err, worker) {
      if (!err && worker) { // Revisar esto
        console.log("tenia " + (worker.points) + " puntos");
        var difference_in_minutes = obtenerMinutos(newlyInsertedRecord.datetime)
        var points_earned = calcularPuntos(difference_in_minutes, newlyInsertedRecord.type)
        worker.points = worker.points +
        console.log("tiene " + (worker.points) + " puntos");
        console.log();
        worker.save(function(err) {
          var SlackBot = require('slackbots');

          // create a bot
          var bot = new SlackBot({
            token: 'xoxb-73513152037-IQt4mOZ8XrEGUMFbuF2lky17', // Add a bot https://my.slack.com/services/new/bot and put the token
            name: 'asistenciometro'
          });

          bot.on('start', function() {
            // more information about additional params https://api.slack.com/methods/chat.postMessage
            // var params = {
            //   icon_emoji: ':asistenciometro_late:'
            // };
            var moment = require('moment');
            var now = moment(newlyInsertedRecord.datetime);
            var current_hour = now.format("HH:mm")
            var message = current_hour + ' | ' + worker.firstName + ' acaba de llegar '
            var message_politicamente_correcto = message
            if(difference_in_minutes > 0) {
                message += difference_in_minutes + ' minutos tarde :('
            } else if (difference_in_minutes < 0) {
                message += difference_in_minutes + ' minutos antes :)'
            }
            message += ' ('
            message_politicamente_correcto += ' ('
            if(points_earned >= 0) {
                message += '+'
                message_politicamente_correcto += '+'
            }
            message += points_earned + ' puntos)'
            message_politicamente_correcto += points_earned + ' puntos)'
            if(points_earned == 20) {
                message = ':asistenciometro_ontime: ' + message
                message_politicamente_correcto = ':asistenciometro_ontime: ' + message_politicamente_correcto
            } else if (points_earned == 10) {
                message = ':asistenciometro_jit: ' + message
                message_politicamente_correcto = ':asistenciometro_jit: ' + message_politicamente_correcto
            } else if (points_earned == -10) {
                message = ':asistenciometro_late: ' + message
                message_politicamente_correcto = ':asistenciometro_late: ' + message_politicamente_correcto
            }
            console.log(message);
            console.log(message_politicamente_correcto);
            // more information about additional params https://api.slack.com/methods/chat.postMessage
            var params = {
              icon_emoji: ':cat:'
            };
            bot.postMessageToChannel('asistenciometro', message_politicamente_correcto, params);
            cb();
          })
        });
      } else {
        cb('worker doesnt exists')
      }
    })
  }
};
